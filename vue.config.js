module.exports = {
  publicPath:
    process.env.NODE_ENV === "production" ? "/metronic8/vue/demo1/" : "/",
  chainWebpack: config => {
    config.module.rules.delete('eslint')
  },
};
